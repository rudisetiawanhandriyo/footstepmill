import { Component, HostListener } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/Authentication.service';
import { Subject } from 'rxjs';
import { UserIdleService } from 'angular-user-idle';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  userActivity;
  userInactive: Subject<any> = new Subject();
  countlast: any;

  url: string;
  dataStasiun: any = [];
  status: boolean;
  mill: string;
  job_desc: string;
  stasiun: string;

  constructor(
    private router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private userIdle: UserIdleService,
    private storage: Storage,
    private alertCtrl: AlertController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      
      //this.statusBar.styleDefault();
      this.statusBar.styleLightContent();
      this.splashScreen.hide();      

      this.storage.get('userInfo').then((response) => {
        if (response) {
          this.router.navigate(['menu/tabs/tabs/home']);
          //Start watching for user inactivity.
          this.userIdle.startWatching();
          
          // Start watching when user idle is starting.
          this.userIdle.onTimerStart().subscribe((count) => {
            this.storage.get('userInfo').then((response) => {
              if(response){
                this.countlast=count;
              }else{
                this.stopWatching();
              }
            });
          });
          
          // Start watch when time is up.
          this.userIdle.onTimeout().subscribe(() => {
            this.showAlertIdleLogout();
            this.authenticationService.logout();
            this.stopWatching();
          });
        } else {
          this.router.navigate(['login']);        
        }
      });      
    });
  }
  
  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();
  }

  @HostListener('touchstart', ['$event']) refreshUserState() {
    this.stop();
    this.restart();
    this.startWatching();    
  }

  //Helper
  async showAlertIdleLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Timeout Info',
      message: 'Please re-login, your session expired',
      buttons: ['OK']
    });
    await alert.present();
  }

}
