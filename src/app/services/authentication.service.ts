import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/timeout';
import { LoadingService } from './loading.service';
import { NavController } from '@ionic/angular';

  
@Injectable()
export class AuthenticationService {
 
  authState = new BehaviorSubject(false);
  url: string;
  dataStasiun: any = [];
  status: boolean;
  mill: string;
  job_desc: string;
  stasiun: string;

  constructor(
    private router: Router,
    private storage: Storage,
    private platform: Platform,
    private toastController: ToastController,
    private alertCtrl: AlertController,
    public http: HttpClient,
    public loading: LoadingService,
    private navCtrl: NavController,
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }
 
  ifLoggedIn() {
    this.storage.get('userInfo').then((response) => {
      if (response) {
        this.authState.next(true);
      }
    });
  }
  
  login(formValue) {
    this.loading.presentCustom("Loading...");
    var username = formValue.value['username'];
    var password = formValue.value['password'];

    this.storage.get('ipServer').then((val) => {
      if(val!=undefined){
        this.url= val.url;

        var link = this.url+'/restapi/fsmill/check_username';
        let body = {
          username:username,
          password:password
        };
        
        this.http.post(link, body, {
          headers: {'Auth-Key':'karyam4s'}
        }).timeout(10000).subscribe((data:any) => {
          if(data.status){
            this.loading.dismiss();
            var dataUser = {
              userId: data.user_id,
              username: data.username,
              nama: data.nama,
              jabatan: data.jabatan,
              department: data.department,
              mill: data.mill,
              login: Date.now(),
            };
                      
            this.storage.set('userInfo', dataUser).then((response) => {
              this.router.navigate(['menu/tabs/tabs/home'], {replaceUrl: true});
              this.authState.next(true);
            });
          }else{
            this.loading.dismiss();
            this.showToast('Username/Password is incorrect');
          }
        }, error => {
          this.loading.dismiss();
          this.showToast("Koneksi gagal, mohon cek koneksi jaringan");
        });
      }
      else{
        this.loading.dismiss();
        this.showToast("URL/IP address belum terisi");
        this.navCtrl.navigateRoot('ip-server');
      }
    });
  }
 
  logout() {
    this.storage.remove('userInfo').then((response) => {
      this.storage.remove('kegiatan');
      this.storage.remove('dataTag');
      this.storage.remove('stasiun');
      this.router.navigate(['login']);
      this.authState.next(false);
    });
  }
 
  isAuthenticated() {
    this.storage.get('userInfo').then((response) => {
      if (response!=null) {
        var loginTime = response.login;
        var selisih = (Date.now() - loginTime);
        if(selisih > 21600000){ //timeout selama 6 jam tidak ada aktivitas
          this.showAlertIdleLogout();
          this.logout();
        }else{
          this.authState = new BehaviorSubject(true);
          this.storage.get('userInfo').then((response) => {
              var dataUser = {
                userId: response.userId,
                username: response.username,
                nama: response.nama,
                jabatan: response.jabatan,
                department: response.department,
                mill: response.mill,
                login: Date.now(),
              };
              this.storage.set('userInfo', dataUser);
          });   
        }
      }else{
        this.logout();
      }
    });
    return this.authState.value;
  }

  // Helper
  async showAlertIdleLogout() {
    const alert = await this.alertCtrl.create({
      header: 'Timeout Info',
      message: 'Please re-login, your session expired',
      buttons: ['OK']
    });
    await alert.present();
  }

  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
 
}