import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/timeout';
import { LoadingService } from './loading.service';
import { ToastController } from '@ionic/angular';

export interface Item {
  id: number;
  stasiun: string;
  mill: string;
  jabatan: string;
  nama: string;
  userId: string;
  latitude: number;
  longitude: number;
  transDate: string;
  checklist: string;
  temuan: string;
  createdDt: number;
  updatedDt: number;
  submitDt: number;
  flag: string;
}

export interface Stasiun {
  stasiun: string,
}

export interface Kegiatan {
  id: number,
  stasiun: string,
  mill: string,
  job_desc: string,
  kegiatan: string,
}

const ITEMS_KEY = 'dataMill';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  itemsSubmit: Item[] = [];
  url: string;

  constructor(private storage: Storage, public http: HttpClient,
    public loading: LoadingService,
    private toastController: ToastController,) { 
    this.storage.get('ipServer').then((val) => {
        this.url= val.url;
    });
  }

  getListStasiun(){
    return this.storage.get("stasiun").then((result: Stasiun[]) => {
      if (!result || result.length === 0) {
        return null;
      }
      let dataStasiun: Stasiun[] = [];
      for (let i of result) {
        dataStasiun.push(i) ;
      }
      return dataStasiun;
    });
  }

  updateDataServer(mill, jabatan, department) {
    this.loading.presentCustom("Download data");
    this.getStasiunMaster(mill).then(res1=>{
      if(res1=="ok"){
        this.getKegiatanMaster(mill, jabatan, department).then(res2=>{
          if(res2=="ok"){
            this.getTagMerah(mill);
          }
        });;
      }
    });
    this.loading.dismiss();
  }

  getStasiunMaster(mill){
    var link = this.url+'/restapi/fsmill/load_stasiun';    
    return new Promise(resolve => {
      this.http.get(link, {headers: {'Auth-Key':'karyam4s', 'mill':mill}})
      .timeout(10000).subscribe((data:any) => {
        this.storage.set("stasiun", JSON.parse(JSON.stringify(data)));
        this.showToast("Stasiun berhasil diupdate");
        resolve("ok");
      }, error => {
        this.showToast("Data gagal diupdate, cek koneksi jaringan");
        resolve("not");
      });
    });
  }

  getKegiatanMaster(mill, jabatan, department){
    var link = this.url+'/restapi/fsmill/load_kegiatan';
    let status = true;
    return new Promise(resolve => {
      this.http.get(link, {headers: {'Auth-Key':'karyam4s', 'mill':mill, 'jabatan':jabatan, 'department':department}})
      .timeout(10000).subscribe((data:any) => {
        this.storage.set("kegiatan", JSON.parse(JSON.stringify(data)));
        this.showToast("Kegiatan berhasil diupdate");
        resolve("ok");
      }, error => {
        this.showToast("Data gagal diupdate, cek koneksi jaringan");
        resolve("not");
      })
    });
  }

  getTagMerah(mill) {
    return new Promise(resolve => {
      let link = this.url+'/restapi/fsmill/load_tag_merah';
      this.http.get(link, {headers: {'Auth-Key':'karyam4s', 'mill':mill}})
      .timeout(10000).subscribe((data:any) => {
        this.storage.set("dataTag", JSON.parse(JSON.stringify(data)));
        this.showToast("Tag merah berhasil diupdate");
        resolve("ok");
      }, error => {
        this.showToast("Data gagal diupdate, cek koneksi jaringan");
        resolve("not");
      });
    });
  }

  getCountTag(){
    return this.storage.get("dataTag").then((result) => {
      if (!result || result.length === 0) {
        return 0;
      }
      return result.length;
    });
  }

  getListTag(){
    return this.getListStasiun().then((res)=>{
      if (!res || res.length === 0) {
        return null;
      }else{
        let newData= []; 
        this.storage.get("dataTag").then((result) => {
          if (!result || result.length === 0) {
            for (let i of res) {
              let jumlah = 0;
              newData.push({
                stasiun: i.stasiun, 
                jumlah: jumlah
              }); 
            }
          }else{
            for (let i of res) {
              let jumlah = 0;
              for (let j of result) {
                if(i.stasiun === j.stasiun){
                  jumlah ++;
                }
              }
              newData.push({
                stasiun: i.stasiun, 
                jumlah: jumlah
              });
            }
          }
        });
        return newData;
      }    
    });
  }

  getListTagDetail(stasiun){
    return this.storage.get("dataTag").then((result) => {
      if (!result || result.length === 0) {
        return null;
      }
      let listData= []; 
      for (let i of result) {
        if(i.stasiun.toLowerCase() == stasiun.toLowerCase()){
          listData.push(i) ;
        }
      }
      return listData;
    });
  }

  //maintenance
  getListTagMaintenance(){
    return this.getListStasiun().then((res)=>{
      if (!res || res.length === 0) {
        return null;
      }else{
        let newData= []; 
        this.storage.get("dataTag").then((result) => {
          if (!result || result.length === 0) {
            for (let i of res) {
              let jumlah = 0;
              newData.push({
                stasiun: i.stasiun, 
                jumlah: jumlah
              }); 
            }
          }else{
            for (let i of res) {
              let jumlah = 0;
              for (let j of result) {
                if(i.stasiun.toLowerCase() == j.stasiun.toLowerCase() && j.maintenance.toLowerCase() == 'ya'){
                  jumlah ++;
                }
              }
              newData.push({
                stasiun: i.stasiun, 
                jumlah: jumlah
              });
            }
          }
        });
        return newData;
      }    
    });
  }

  getListTagDetailMaintenance(stasiun){
    return this.storage.get("dataTag").then((result) => {
      if (!result || result.length === 0) {
        return null;
      }
      let listData= []; 
      for (let i of result) {
        if(i.stasiun.toLowerCase() == stasiun.toLowerCase() && i.maintenance.toLowerCase() == 'ya'){
          listData.push(i) ;
        }
      }
      return listData;
    });
  }

  // load kegiatan
  getKegiatan(stasiun){
    return this.storage.get("kegiatan").then((result: Kegiatan[]) => {
      if (!result || result.length === 0) {
        return false;
      }
      let dataKegiatan: Kegiatan[] = [];
      for (let i of result) {
        if (stasiun.toLowerCase() == i.stasiun.toLowerCase()) {
          dataKegiatan.push(i) ;
        } else {
        }
      }
      if(dataKegiatan.length != 0){
        return dataKegiatan;
      }else{
        return false;
      }
    });
  }

  getListKegiatanDetail(stasiun){
    return this.storage.get("kegiatan").then((result) => {
      if (!result || result.length === 0) {
        return null;
      }
      let listData= []; 
      for (let i of result) {
        if(i.stasiun == stasiun){
          listData.push(i) ;
        }
      }
      return listData;
    });
  }

  // CREATE
  addItem(item: Item): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (items) {
        items.push(item);
        return this.storage.set(ITEMS_KEY, items);
      } else {
        return this.storage.set(ITEMS_KEY, [item]);
      }
    });
  }

  // READ
  getItems(): Promise<Item[]> {
    return this.storage.get(ITEMS_KEY);
  }

  // LOAD
  getItem(id){
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }      
      let dataItem: Item[] = [];
      for (let i of items) {
        if (id == i.id) {
          dataItem.push(i) ;
        } else {
        }
      }      
      return dataItem;
    });
  }

  // load item belum disubmit
  getItemsNotSubmit(){
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }
      let dataItem: Item[] = [];      
      for (let i of items) {
        if (i.flag != '1') {
          dataItem.push(i) ;
        } else {
        }
      }
      return dataItem;
    });
  }

  // load item sudah disubmit
  getItemsSubmit(){
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }      
      let dataItem: Item[] = [];      
      for (let i of items) {
        if (i.flag == '1') {
          dataItem.push(i) ;
        } else {
        }
      }
      return dataItem;
    });
  }
 
  // UPDATE
  updateItem(item: Item): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }      
      let newItems: Item[] = []; 
      for (let i of items) {
        if (i.id === item.id) {
          newItems.push(item);
        } else {
          newItems.push(i);
        }
      }      
      return this.storage.set(ITEMS_KEY, newItems);
    });
  }

  // UPDATE FLAG ITEMS BY ID
  updateFlagItems(id): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }
      for (let i of items) {
        if (i.id === id) {
          i.submitDt = Date.now();
          i.flag = '1';
        }        
      }      
      return this.storage.set(ITEMS_KEY, items);
    });    
  }
 
  // DELETE
  deleteItem(id: number): Promise<Item> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      } 
      let toKeep: Item[] = []; 
      for (let i of items) {
        if (i.id !== id) {
          toKeep.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, toKeep);
    });
  }

  // DELETE DATA SUBMIT
  deleteItemsSubmit(): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }      
      let toKeep: Item[] = [];      
      for (let i of items) {
        if (i.flag === "0") {
          toKeep.push(i);
        }
      }      
      return this.storage.set(ITEMS_KEY, toKeep);
    });
  }

  // DELETE DATA SUBMIT AFTER NEXT DAY
  deleteItemsSubmitOldDate(): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0) {
        return null;
      }
      let dataItem: Item[] = [];
      var tglSekarang = new Date().setHours(0,0,0,0);
      for (let i of items) {
        if (i.submitDt >= tglSekarang || i.submitDt == 0) {
          dataItem.push(i) ;
        }
      }
      return this.storage.set(ITEMS_KEY, dataItem);
    });
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
