import { Injectable } from '@angular/core';
//import { CanActivate } from '@angular/router';
import { AuthenticationService } from './Authentication.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
 
@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(
      public authenticationService: AuthenticationService
        ) {}
 
    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        
      return this.authenticationService.isAuthenticated();
      
    }
}