import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../app/services/Auth-guard.service';

const routes: Routes = [
   { path: '', redirectTo: 'home', pathMatch: 'full'},
   { path: 'add-data/:stasiun', loadChildren: './pages/add-data/add-data.module#AddDataPageModule', canActivate: [AuthGuardService] },
   { path: 'update-data/:id', loadChildren: './pages/update-data/update-data.module#UpdateDataPageModule', canActivate: [AuthGuardService] },
   { path: 'detail-data/:id', loadChildren: './pages/detail-data/detail-data.module#DetailDataPageModule', canActivate: [AuthGuardService] },
   { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
   { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
   { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuardService] },
   { path: 'ip-server', loadChildren: './pages/ip-server/ip-server.module#IpServerPageModule'},
   { path: 'kegiatan', loadChildren: './pages/kegiatan/kegiatan.module#KegiatanPageModule', canActivate: [AuthGuardService] },
   { path: 'listtag', loadChildren: './pages/listtag/listtag.module#ListtagPageModule', canActivate: [AuthGuardService] },
   { path: 'detailtag/:stasiun', loadChildren: './pages/detailtag/detailtag.module#DetailtagPageModule', canActivate: [AuthGuardService] },
   { path: 'viewtag/:stasiun', loadChildren: './pages/viewtag/viewtag.module#ViewtagPageModule', canActivate: [AuthGuardService] },
   { path: 'detailkegiatan/:stasiun', loadChildren: './pages/detailkegiatan/detailkegiatan.module#DetailkegiatanPageModule', canActivate: [AuthGuardService] },
   { path: 'list-maintenance', loadChildren: './pages/list-maintenance/list-maintenance.module#ListMaintenancePageModule', canActivate: [AuthGuardService] },
   { path: 'detail-maintenance/:stasiun', loadChildren: './pages/detail-maintenance/detail-maintenance.module#DetailMaintenancePageModule', canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [
    //RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
