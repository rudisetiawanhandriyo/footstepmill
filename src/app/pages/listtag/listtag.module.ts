import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListtagPageRoutingModule } from './listtag-routing.module';

import { ListtagPage } from './listtag.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListtagPageRoutingModule
  ],
  declarations: [ListtagPage]
})
export class ListtagPageModule {}
