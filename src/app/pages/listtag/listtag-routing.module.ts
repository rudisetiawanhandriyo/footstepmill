import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListtagPage } from './listtag.page';

const routes: Routes = [
  {
    path: '',
    component: ListtagPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListtagPageRoutingModule {}
