import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListtagPage } from './listtag.page';

describe('ListtagPage', () => {
  let component: ListtagPage;
  let fixture: ComponentFixture<ListtagPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListtagPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListtagPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
