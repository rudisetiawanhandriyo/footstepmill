import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { StorageService } from '../../services/storage.service';
import { ToastController} from '@ionic/angular';
import 'rxjs/add/operator/timeout';
import { LoadingService } from '../../services/loading.service';
import { NavController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-listtag',
  templateUrl: './listtag.page.html',
  styleUrls: ['./listtag.page.scss'],
})

export class ListtagPage implements OnInit {

  url: string;
  mill: string;
  listTag: any = [];

  constructor(private storage: Storage, private storageService: StorageService, private toastController: ToastController,
    public http: HttpClient,
    public loading: LoadingService,
    private navCtrl: NavController) { 
    this.storage.get('ipServer').then((val) => {
      this.url= val.url;
    });
    this.storage.get('userInfo').then((val) => {
      this.mill= val.mill;
    });
  }

  ngOnInit() {
    this.loadListTag();
  }

  loadListTag(){
    this.storageService.getListTag().then(items => {
      if(items!=null){
        this.listTag = items.reverse();
      }
    });
  }

  updateData(){
    this.loading.present();
    let link = this.url+'/restapi/fsmill/load_tag_merah';
    return new Promise(resolve => {
      this.http.get(link, {headers: {'Auth-Key':'karyam4s', 'mill':this.mill}})
      .timeout(10000).subscribe((data:any) => {
        this.storage.set("dataTag", JSON.parse(JSON.stringify(data)));
        this.showToast("Data berhasil diupdate");
        this.loadListTag();
        this.loading.dismiss();
        resolve();
      }, error => {
        this.showToast("Data gagal diupdate, cek koneksi jaringan");
        this.loading.dismiss();
        resolve();
      });
    });
  }

  detailPage(stasiun:any) {
    this.navCtrl.navigateRoot('detailtag/'+stasiun);
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'middle',
    });
    toast.present();
  }

}
