import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { StorageService } from '../../services/storage.service';
import { ToastController} from '@ionic/angular';
import { LoadingService } from '../../services/loading.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-list-maintenance',
  templateUrl: './list-maintenance.page.html',
  styleUrls: ['./list-maintenance.page.scss'],
})

export class ListMaintenancePage implements OnInit {

  listTagMaintenance: any = [];

  constructor(private storage: Storage, private storageService: StorageService, private toastController: ToastController,
    public loading: LoadingService,
    private navCtrl: NavController) { 
  }

  ngOnInit() {
    this.loadListTagMaintenance();
  }

  loadListTagMaintenance(){
    this.storageService.getListTagMaintenance().then(items => {
      if(items!=null){
        this.listTagMaintenance = items.reverse();
      }
    });
  }

  detailPage(stasiun:any) {
    this.navCtrl.navigateRoot('detail-maintenance/'+stasiun);
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'middle',
    });
    toast.present();
  }

}
