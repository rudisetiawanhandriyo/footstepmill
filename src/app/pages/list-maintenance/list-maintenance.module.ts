import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListMaintenancePageRoutingModule } from './list-maintenance-routing.module';

import { ListMaintenancePage } from './list-maintenance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListMaintenancePageRoutingModule
  ],
  declarations: [ListMaintenancePage]
})
export class ListMaintenancePageModule {}
