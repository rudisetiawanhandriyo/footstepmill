import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListMaintenancePage } from './list-maintenance.page';

const routes: Routes = [
  {
    path: '',
    component: ListMaintenancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListMaintenancePageRoutingModule {}
