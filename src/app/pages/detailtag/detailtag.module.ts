import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailtagPageRoutingModule } from './detailtag-routing.module';

import { DetailtagPage } from './detailtag.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailtagPageRoutingModule
  ],
  declarations: [DetailtagPage]
})
export class DetailtagPageModule {}
