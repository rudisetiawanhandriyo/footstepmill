import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailtagPage } from './detailtag.page';

const routes: Routes = [
  {
    path: '',
    component: DetailtagPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailtagPageRoutingModule {}
