import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailtagPage } from './detailtag.page';

describe('DetailtagPage', () => {
  let component: DetailtagPage;
  let fixture: ComponentFixture<DetailtagPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailtagPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailtagPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
