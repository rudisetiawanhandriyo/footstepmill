import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-detailtag',
  templateUrl: './detailtag.page.html',
  styleUrls: ['./detailtag.page.scss'],
})

export class DetailtagPage implements OnInit {

  stasiun = null;
  listData = [];

  constructor(private activatedRoute: ActivatedRoute,
    private storageService: StorageService,
    private photoViewer: PhotoViewer) { }

  ngOnInit() {
    this.stasiun = this.activatedRoute.snapshot.paramMap.get('stasiun');
    // Get the information from the API
    this.storageService.getListTagDetail(this.stasiun).then(items => {
      if(items!=null){
        this.listData = items.reverse();
      }
    });
  }

  viewFoto(src){
    this.photoViewer.show(src);
  }

}
