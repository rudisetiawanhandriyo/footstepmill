import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDataPage } from './list-data.page';

describe('ListDataPage', () => {
  let component: ListDataPage;
  let fixture: ComponentFixture<ListDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDataPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
