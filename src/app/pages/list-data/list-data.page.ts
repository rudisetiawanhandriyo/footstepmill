import { Component, ViewChild  } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { StorageService, Item } from '../../services/storage.service';
import { LoadingService } from '../../services/loading.service';
import { Platform, ToastController, IonList  } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { File, FileEntry } from '@ionic-native/file/ngx';
import 'rxjs/add/operator/timeout';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-list-data',
  templateUrl: './list-data.page.html',
  styleUrls: ['./list-data.page.scss'],
})

export class ListDataPage  {

  currentStatus: string;
  itemsOpen: Item[] = [];
  itemsSubmit: Item[] = [];
  itemsNotSubmit: Item[] = [];
  newItem: Item = <Item>{};

  public idData: any = [];

  dataOpen: boolean;
  dataSubmit: boolean;
  url: string;
  segment: string;
  searchTerm: string = '';
  userId: string;

  status: boolean;

  images = [];
  formDataUpload = new FormData();

  @ViewChild('mylist', { static: false })mylist: IonList;

  constructor(private storage: Storage, private storageService: StorageService, private plt: Platform, private toastController: ToastController, public http: HttpClient,
    public loading: LoadingService,
    private navCtrl: NavController,
    public alertCtrl: AlertController,
    private router: Router,
    private file: File) {
      this.storage.get('ipServer').then((val) => {
        this.url= val.url;
      });
      this.storage.get('userInfo').then((val) => {
        this.userId= val.userId;
      });
      this.segment = "segment1";
  }

  doRefreshOpen(event) {
    this.dataOpen = false;
    this.loadItemsOpen();
    event.target.complete();
  }

  doRefreshSubmit(event) {
    this.dataSubmit = false;
    this.loadItemsSubmit();
    event.target.complete();
  }

  ionViewWillEnter() {
    this.dataOpen = false; 
    this.dataSubmit = false; 
    this.itemsOpen = [];
    this.itemsSubmit = [];
    this.loadItemsOpen();
    this.loadItemsSubmit();
  }
  
  // READ
  loadItemsOpen() { 
    this.storageService.getItemsNotSubmit().then(items => {
      if(items!=null){
        this.dataOpen = true;
        this.itemsOpen = items.reverse();
      }else{
        this.dataOpen = false;
      }
    });
  }

  loadItemsSubmit() {   
    this.storageService.getItemsSubmit().then(items => {
      if(items!=null){
        this.dataSubmit = true;
        this.itemsSubmit = items.reverse();
      }else{
        this.dataSubmit = false;
      }
    });
  }

  update_page(id:any) {
    this.navCtrl.navigateRoot('update-data/'+id);
  }

  detail_page(id:any){
    this.navCtrl.navigateRoot('detail-data/'+id);
  }

  // DELETE
  deleteItem(id:any) {
    this.loading.present();
    this.storageService.deleteItem(id).then(item => {
      this.showToast('data has been deleted');
      this.mylist.closeSlidingItems(); // Fix or sliding is stuck afterwards
      this.ionViewWillEnter();
    });
    this.loading.dismiss();
  }

  async showAlertConfirmDelete() {
    const alert = await this.alertCtrl.create({
      header: 'Delete All Submitted Data',
      message: 'Are You Sure ?',
      inputs: [],
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Yes',
          handler: data => {
            //load data submit
            this.storageService.getItemsSubmit().then(items => {
              this.itemsSubmit = items.reverse();
              if(this.itemsSubmit.length!=0){
                this.loading.present();
                this.storageService.deleteItemsSubmit().then((result) => {
                  this.ionViewWillEnter(); 
                });
                this.showToast("data has been deleted");
                this.loading.dismiss();
              }else{
                this.showToast("no data deleted");
              }
            }); 
          }
        }
      ]
    });
    await alert.present();
  }

  //submit data ke server
  submitData(id:any) {
    this.storageService.getItem(id).then((item : Item[]) => {
      let dataForm = JSON.stringify(item[0]); 
      let assignmentData = item[0].checklist;      
      let temuanData = item[0].temuan;
      var listFoto = [];
      let obj = JSON.parse(JSON.stringify(assignmentData));
      obj.forEach(item => {
        if(item.photo!='-'){
          listFoto.push(item.photo);
        }
      });
      if(temuanData!=''){    
        let obj2 = JSON.parse(JSON.stringify(temuanData));
        obj2.forEach(item => {
          if(item.photo!='-'){
            listFoto.push(item.photo);
          }
        });    
      }
      if(listFoto.length > 0){
        this.submitDataFoto(listFoto, dataForm, id);
      }else{
        this.submitDataForm(dataForm).then((result)=>{
          if(result=="ok"){
            this.storageService.updateFlagItems(id).then(()=>{
              this.ionViewWillEnter();
            });
          }
        });
      }      
    });
   }

  submitDataForm(dataForm){
    let message = 'Uploading data'; 
    this.loading.presentCustom(message);
    let link = this.url+'/restapi/fsmill/submit_data';
    return new Promise((resolve, reject) => { this.http.post(link, dataForm, {
        headers: {'Auth-Key':'karyam4s', 'userid' : this.userId}
      }).timeout(10000).toPromise().then((data:any) => {
        this.loading.dismiss();
        this.showToast("Data berhasil diupload");
        resolve("ok");
      }, error => {
        this.loading.dismiss();
        this.showToast("Data gagal diupdate, cek koneksi jaringan");
        reject("not");
      });
    });
  }

  async submitDataFoto(listFoto, dataForm, id){
    this.loading.presentCustom("Proses Upload Foto");
    let promises: any[] = []
    for(let item of listFoto){
      let a = await this.uplodGambar(item);
      promises.push(a);
    }    
    await Promise.all(promises).then((res) => {
      this.loading.dismiss();
      this.showToast("Upload Foto Sukses");
      this.submitDataForm(dataForm).then((result)=>{
        if(result=="ok"){
          this.storageService.updateFlagItems(id).then(()=>{
            this.ionViewWillEnter();
          });
        }
      });
    },(firstErr) => {
      this.loading.dismiss();
      this.showToast(firstErr);
    });
  }

  async uplodGambar(nama){
    let filePath = this.file.dataDirectory + nama;  
    return new Promise((resolve, reject) => {  
      this.file.resolveLocalFilesystemUrl(filePath).then(entry => {
        ( < FileEntry > entry).file(file => {
          const reader = new FileReader();
          reader.onload = () => {
              const formFoto = new FormData();
              const imgBlob = new Blob([reader.result], {
              type: file.type
            });
            formFoto.append('file', imgBlob, file.name);             
            let link = this.url+'/restapi/fsmill/upload_photo'; 
            this.http.post(link, formFoto).timeout(10000)
              .subscribe((something: any) => {
                  resolve("ok")
              }, (errorResponse: any) => {
                  this.showToast("Koneksi terputus, mohon upload kembali");
                  reject(errorResponse);
                  this.loading.dismiss();
              });   
          };
          reader.readAsArrayBuffer(file);
        })
      }).catch(err => {
          this.showToast('Error while reading file.');
      })
    })
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
    });
    toast.present();
  }

}
