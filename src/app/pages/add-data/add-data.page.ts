import { Component, ViewChild, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { StorageService, Item } from '../../services/storage.service';
import { Platform, ToastController, IonList } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { LoadingService } from '../../services/loading.service';

import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

import { File } from '@ionic-native/file/ngx';

import { ModalController } from '@ionic/angular';
import { ViewtagPage } from '../../pages/viewtag/viewtag.page';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

@Component({
  selector: 'app-add-data',
  templateUrl: './add-data.page.html',
  styleUrls: ['./add-data.page.scss'],
})

export class AddDataPage implements OnInit{

  items: Item[] = [];
  newItem: Item = <Item>{};
  
  nama: string;
  userId: string;
  jabatan: string;
  department: string;
  mill: string;
  stasiun: string;
  data: any;
  form: FormGroup;
  assignmentList: FormArray;  
  temuanList: FormArray;
  tanggal: number;
  tanggal2: any;

  originalImage = null;
  blobImage = null;
  
  latitude: number;
  longitude: number;

  jmlTag: number;

  listFoto:string[]=[];
  listFotoTemuan:string[]=[];
  
  @ViewChild('mylist', { static: false })mylist: IonList;

  constructor(private storageService: StorageService, private storage: Storage, private plt: Platform, private toastController: ToastController,
    private fb: FormBuilder, 
    private camera: Camera,
    private photoViewer: PhotoViewer,
    public loading: LoadingService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    public alertCtrl: AlertController,
    private file: File,
    public modalController: ModalController,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy
    ) {

    this.tanggal = Date.now();
    this.tanggal2 = new Date();

    this.newItem = <Item>{};
    this.stasiun = this.activatedRoute.snapshot.paramMap.get('stasiun');
    this.form = this.fb.group({
      stasiun: [this.stasiun, Validators.compose([Validators.required])],
      transDate: [null, Validators.compose([Validators.required])],
      assignment: this.fb.array([]),
      temuan: this.fb.array([])
    });
  }

  ngOnInit(){
    this.checkGPSPermission();

    this.storage.get('userInfo').then((val) => {
      this.userId = val.userId;
      this.nama= val.nama;
      this.jabatan= val.jabatan;
      this.department= val.department;
      this.mill= val.mill;

      this.storageService.getListTagDetail(this.stasiun).then((items)=>{
        if (!items || items.length === 0) {
          this.jmlTag = 0;
        }else{
          this.jmlTag = items.length;
        }
      });

      this.storageService.getKegiatan(this.stasiun).then(res => {
         this.assignmentList = this.form.get('assignment') as FormArray;
         if(res != false){
           const formArray = this.form.get('assignment') as FormArray;
           res.forEach(item => {
             formArray.push(this.createForms(item));
           });
         }else{
            this.showToast("Ceklist kegiatan tidak ditemukan, update kegiatan terlebih dahulu");
            this.navCtrl.navigateRoot('kegiatan');
         }
       });
       this.temuanList = this.form.get('temuan') as FormArray;
    });    
  }

  ionViewWillEnter(){
    this.form.patchValue({
      transDate: this.tanggal,
    })
  }

   //Check if application having GPS access permission  
   checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {
          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert("Mohon berikan akses aplikasi untuk lokasi")
              this.navCtrl.navigateRoot('menu/tabs/tabs/home');
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates();
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    var option = {
      enableHighAccuracy: true, 
    };
    this.geolocation.getCurrentPosition(option).then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
     }).catch((error) => {
      this.latitude = 0;
      this.longitude = 0;
     });
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: ViewtagPage,
      componentProps: {
        "paramStasiun": this.stasiun,
      }
    }); 
    return await modal.present();
  }

  //create formgroup with data
  createForms(assignment): FormGroup {
    let formGroup: FormGroup = new FormGroup(
      {
        'id' : new FormControl(assignment.id, Validators.required),
        'kegiatan': new FormControl(assignment.kegiatan, Validators.required),
        'kondisi': new FormControl(assignment.kondisi, Validators.required),
        'keterangan': new FormControl(assignment.keterangan, Validators.required),
        'photo': new FormControl(assignment.photo, Validators.required),
      }
    );
    return formGroup;
  }

  // get the formgroup under contacts form array
  getAssignmentsFormGroup(index): FormGroup {
    const formGroup = this.assignmentList.controls[index] as FormGroup;
    return formGroup;
  }

  kondisiChange(index: string, event: any){
    var kondisi = event.target.value;
    if(kondisi=="yes" || kondisi=="skip"){
      this.assignmentList.controls[index].controls['keterangan'].setValue("-");
      this.assignmentList.controls[index].controls['photo'].setValue("-");
    }else{
      this.assignmentList.controls[index].controls['keterangan'].setValue("");
      this.assignmentList.controls[index].controls['photo'].setValue("");
    }
  }

  // bagian temuan
  createTemuan(): FormGroup{
    return this.fb.group({
      keterangan: [null, Validators.compose([Validators.required])], 
      photo: [null, Validators.compose([Validators.required])],
    });
  }
  
  addTemuan(){
    this.temuanList.push(this.createTemuan());
  }

  removeTemuan(i: number) : void{
    const control = <FormArray>this.form.get('temuan');
    control.removeAt(i);
  }

  // get the formgroup under contacts form array
  getTemuanFormGroup(index): FormGroup {
    const formGroup = this.temuanList.controls[index] as FormGroup;
    return formGroup;
  }

  // CREATE
  addItem(form) {
    this.loading.present();
    this.newItem.createdDt = Date.now();
    this.newItem.id = Date.now();
    this.newItem.stasiun = form.value.stasiun;
    this.newItem.transDate = form.value.transDate;
    this.newItem.mill = this.mill;
    this.newItem.nama = this.nama;
    this.newItem.jabatan = this.jabatan+' '+this.department;   
    this.newItem.userId = this.userId;  
    this.newItem.latitude = this.latitude;
    this.newItem.longitude = this.longitude;
    this.newItem.submitDt = 0;
    this.newItem.flag = "0";
    this.newItem.checklist = form.value.assignment;
    this.newItem.temuan = form.value.temuan;
    
    this.storageService.addItem(this.newItem).then(item => {
      this.navCtrl.navigateRoot('/menu/tabs/tabs/list');
    });
    this.loading.dismiss();
    this.showToast("Data berhasil disimpan");
  }

  //generate filename foto
  createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName = n + this.userId + ".jpg";
    return newFileName;
  }

  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  //mengambil foto
  takePhoto(index: string) {
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false,
      allowEdit : false,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.originalImage = 'data:image/jpeg;base64,' + imagePath;
      let nameImage = this.createFileName();
      this.listFoto[index] = this.originalImage;
      let realData = this.originalImage.split(",")[1];
      let blob = this.b64toBlob(realData, 'image/jpeg');      
    
      this.file.writeFile(this.file.dataDirectory, nameImage, blob).then(success => {
        this.assignmentList.controls[index].controls['photo'].setValue(nameImage);
      }, error => {
        this.showToast('Error while storing file.');
      });
      
    }, (err) => {
      // Handle error
      console.log("Camera issue:" + err);
    });
  }

  //mengambil foto temuan
  takePhotoTemuan(index: string) {
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false,
      allowEdit : false,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.originalImage = 'data:image/jpeg;base64,' + imagePath;
      let nameImage = this.createFileName();
      this.listFotoTemuan[index] = this.originalImage;
      let realData = this.originalImage.split(",")[1];
      let blob = this.b64toBlob(realData, 'image/jpeg');      
    
      this.file.writeFile(this.file.dataDirectory, nameImage, blob).then(success => {
        this.temuanList.controls[index].controls['photo'].setValue(nameImage);
      }, error => {
        this.showToast('Error while storing file.');
      });
      
    }, (err) => {
      // Handle error
      console.log("Camera issue:" + err);
    });
  }

  viewPhoto(index: string) {
    this.photoViewer.show(this.listFoto[index]);
  }

  viewPhotoTemuan(index: string) {
    this.photoViewer.show(this.listFotoTemuan[index]);
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000, 
    });
    toast.present();
  }

}