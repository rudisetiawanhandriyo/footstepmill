import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IpServerPage } from './ip-server.page';

describe('IpServerPage', () => {
  let component: IpServerPage;
  let fixture: ComponentFixture<IpServerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IpServerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpServerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
