import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ToastController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-ip-server',
  templateUrl: './ip-server.page.html',
  styleUrls: ['./ip-server.page.scss'],
})
export class IpServerPage implements OnInit {

  public form: FormGroup;
  public url: string;

  constructor(private storage: Storage, public toastController: ToastController,private formBuilder: FormBuilder) { 
      this.form = this.formBuilder.group({
    });
  }

  ngOnInit() {
    this.storage.get('ipServer').then((val) => {
      this.url= val.url;
    });
  }

  save(form){
    var url = form.value.url;
    var data = {
      url: url,
    };
    this.storage.set('ipServer', data).then((response) => {
      this.showToast("URL IP Address updated");
    });
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
