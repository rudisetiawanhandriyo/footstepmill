import { Component, ViewChild, OnInit } from '@angular/core';
import { StorageService, Item } from '../../services/storage.service';
import { Platform, ToastController, IonList } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/timeout';
import { LoadingService } from '../../services/loading.service';
import { File } from '@ionic-native/file/ngx'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  url: string;
  mill: string;
  jabatan: string;
  department: string;
  data:any = {};
  currentStatus: string;
  items: Item[] = [];
  newItem: Item = <Item>{};
  countOpen: number;
  countSubmmit: number;
  countTag: number;
  
  @ViewChild('mylist', { static: false })mylist: IonList;

  constructor(private storageService: StorageService, 
    private storage: Storage, 
    private plt: Platform, 
    private toastController: ToastController,
    public alertCtrl: AlertController,
    private router: Router,
    public http: HttpClient,
    public loading: LoadingService,
    private file: File,
     ) {    
    this.mill = "";
    this.countOpen = 0;
    this.countSubmmit = 0;
    this.countTag = 0;
  }

  ngOnInit(){
    this.storageService.deleteItemsSubmitOldDate();
    this.storage.get('ipServer').then((val) => {
      this.url= val.url;
      this.storage.get('userInfo').then((val) => {
        this.mill= val.mill;
        this.jabatan= val.jabatan;
        this.department= val.department;
        this.storageService.updateDataServer(this.mill, this.jabatan, this.department);
      });
    });
  }

  ionViewWillEnter(){
    this.deleteFoto();
    setTimeout(()=>{
      this.loadItemsOpen();
      this.loadItemsSubmit(); 
      this.loadTagMerah();
    }, 5000);
  }

  // READ
  loadItemsOpen() {   
    this.storageService.getItemsNotSubmit().then(items => {
      if(items!=null){
        this.countOpen = items.length
      }
    });
  }

  loadItemsSubmit() {   
    this.storageService.getItemsSubmit().then(items => {
      if(items!=null){
        this.countSubmmit = items.length;
      }
    });
  }

  loadTagMerah() {   
    this.storageService.getCountTag().then(res => {
      this.countTag = res;
    });
  }

  getDataTag(){
    this.storageService.getTagMerah(this.mill).then(res =>{
      this.loadTagMerah();
    });
  }

  viewListTag(){
    this.router.navigate(['listtag']);
  }

  //DELETE FOTO YANG TIDAK DIPAKAI
  deleteFoto(){
    var listFoto = []; var listFotoData = [];
    this.file.listDir(this.file.dataDirectory,'').then(result=>{
      if(result!=undefined){
        for(var interval=0; interval<result.length; interval++){
          if(result[interval].isFile){
            listFoto.push(result[interval].name);
          }        
        }
      }
      this.storageService.getItems().then(results =>{
        if(results!=null){
          for (let i of results) {
            var assignmentData = i.checklist;
            if(assignmentData.length!=0){
              var obj = JSON.parse(JSON.stringify(assignmentData));
              obj.forEach(item => {
                listFotoData.push(item.photo);
              });
            }
          }
        }
        if(results!=null){
          for (let i of results) {
            var assignmentData = i.temuan;
            if(assignmentData.length!=0){
              var obj = JSON.parse(JSON.stringify(assignmentData));
              obj.forEach(item => {
                listFotoData.push(item.photo);
              });
            }
          }
        }
        //proses hapus foto tidak dipakai
        for(let i of listFoto){
          if(!(listFotoData.includes(i))){
            this.file.removeFile(this.file.dataDirectory, i);
          }        
        }
      });
    }); 
  }
  
  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}