import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { AlertController } from '@ionic/angular';
import { StorageService } from '../../services/storage.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  qrId: any;
	stasiun: any;
  scanned: boolean;

  constructor(public barcodeCtrl: BarcodeScanner,
    public alertCtrl: AlertController,
    private navCtrl: NavController,
    private storageService: StorageService, private storage: Storage) { 
  }

  ngOnInit() {
  }

  scan(){
    this.storage.get('stasiun').then((val) => {
      if(val===null || val.length===0){
        this.showAlertStasisun();
      }else{
        const options: BarcodeScannerOptions = {
          preferFrontCamera: false,
          showFlipCameraButton: false,
          showTorchButton: true,
          torchOn: false,
          prompt: 'Place a QR code inside the scan area',
          resultDisplayDuration: 500,
          formats: 'QR_CODE,PDF_417',
          orientation: 'portarit',
        };
    
        this.barcodeCtrl.scan(options).then(barcodeData => {
          try{
            var obj = JSON.parse(barcodeData.text);
            if(obj.key=="MillFootStep"){
              this.qrId = obj.id;  
              this.stasiun = obj.stasiun;
              this.navCtrl.navigateRoot('add-data/'+this.stasiun);
            }else{
              this.presentAlert();
            }
          }catch{
          }
        }).catch(err => {
          console.log('Error', err);
        });
      }
    });
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Info',
      message: 'QR tidak dikenal',
      buttons: ['OK']
    });

    await alert.present();
  }

  async showAlertStasisun() {
    const alert = await this.alertCtrl.create({
      header: 'Info',
      message: 'Data stasiun tidak ada, mohon update stasiun dulu',
      inputs: [
      ],
      buttons: [
       {
          text: 'OK',
          handler: data => {
            this.navCtrl.navigateRoot('kegiatan');
          }
        }
      ]
    });
    alert.present();
  }
}

