import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';
import { TabsPageRoutingModule } from '../tabs/tabs-routing.module';

/*const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: '../home/home.module#HomePageModule'
      },
      
      {
        path: 'add',
        loadChildren: '../add-data/add-data.module#AddDataPageModule'
      },
      {
        path: 'list',
        loadChildren: '../list-data/list-data.module#ListDataPageModule'
      },
      {
        path: 'update/:id',
        loadChildren: '../update-data/update-data.module#UpdateDataPageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/home',
    pathMatch: 'full'
  }
];*/

@NgModule({
  imports:
    [
      IonicModule,
      CommonModule,
      FormsModule,
      //RouterModule.forChild(routes)
      TabsPageRoutingModule
    ],
  declarations:
    [
      TabsPage
    ]
})
export class TabsPageModule {}