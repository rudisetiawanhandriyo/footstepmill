import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthenticationService } from '../../services/Authentication.service';
import { Storage } from '@ionic/storage';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { NavController } from '@ionic/angular';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string;
  password: string;
  public loginForm: FormGroup;
  isActiveToggleTextPassword: Boolean = true;
  versionNumber: string;

  constructor(private authService: AuthenticationService,private formBuilder: FormBuilder,
    private storage: Storage,
    public appVersion: AppVersion,
    private navCtrl: NavController) { 
      this.loginForm = this.formBuilder.group({
    });
    this.appVersion.getVersionNumber().then((value) => {
      this.versionNumber = value;
    });
  }
 
  ngOnInit() {
  }

  ionViewWillEnter(){
    this.authService.ifLoggedIn();
    this.username = "";
    this.password = "";
  }
 
  login(form){
    this.authService.login(form);
  }

  public toggleTextPassword(): void{
    this.isActiveToggleTextPassword = (this.isActiveToggleTextPassword==true)?false:true;
  }

  public getType() {
    return this.isActiveToggleTextPassword ? 'password' : 'text';
  }

  configuration(){
    this.navCtrl.navigateRoot('ip-server');
  }
  
}