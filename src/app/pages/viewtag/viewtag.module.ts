import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewtagPageRoutingModule } from './viewtag-routing.module';

import { ViewtagPage } from './viewtag.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewtagPageRoutingModule
  ],
  declarations: [ViewtagPage]
})
export class ViewtagPageModule {}
