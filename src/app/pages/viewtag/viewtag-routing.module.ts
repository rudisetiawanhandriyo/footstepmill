import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewtagPage } from './viewtag.page';

const routes: Routes = [
  {
    path: '',
    component: ViewtagPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewtagPageRoutingModule {}
