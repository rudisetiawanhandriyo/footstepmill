import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewtagPage } from './viewtag.page';

describe('ViewtagPage', () => {
  let component: ViewtagPage;
  let fixture: ComponentFixture<ViewtagPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewtagPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewtagPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
