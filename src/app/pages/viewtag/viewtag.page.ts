import { Component, OnInit } from '@angular/core';
import { StorageService, Item } from '../../services/storage.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-viewtag',
  templateUrl: './viewtag.page.html',
  styleUrls: ['./viewtag.page.scss'],
})
export class ViewtagPage implements OnInit {

  stasiun = null;
  listData = [];

  constructor(private modalController: ModalController,
    private navParams: NavParams,
    private storageService: StorageService) { }

  ngOnInit() {
    //this.modalTitle = this.navParams.data.paramTitle;
    this.stasiun = this.navParams.data.paramStasiun;
    // Get the information from the API
    this.storageService.getListTagDetail(this.stasiun).then(items => {
      if(items!=null){
        this.listData = items.reverse();
      }
    });
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
}
