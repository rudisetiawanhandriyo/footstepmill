import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { AuthenticationService } from '../../services/Authentication.service';
import { Storage } from '@ionic/storage';
import { StorageService } from '../../services/storage.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
 
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  selectedPath = '';
  username: string;
  nama: string;
  jabatan: string;
  department: string;
  mill: string;
  url: string;
  versionNumber: string;

  constructor(private router: Router, private authService: AuthenticationService,
    private storage: Storage,
    private storageService: StorageService,
    public appVersion: AppVersion) {
  }
 
  ngOnInit() {
  }

  ionViewWillEnter(){
    this.router.events.subscribe((event: RouterEvent) => {      
      this.appVersion.getVersionNumber().then((value) => {
        this.versionNumber = value;
      });
    });
    this.storage.get('userInfo').then((val) => {
      this.username= val.username;
      this.nama = val.nama;
      this.jabatan= val.jabatan;
      this.department= val.department;
      this.mill= val.mill;
    });
  }

  logoutUser(){
    this.authService.logout();
  }
 
}