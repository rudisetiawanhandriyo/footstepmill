import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailkegiatanPage } from './detailkegiatan.page';

describe('DetailkegiatanPage', () => {
  let component: DetailkegiatanPage;
  let fixture: ComponentFixture<DetailkegiatanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailkegiatanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailkegiatanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
