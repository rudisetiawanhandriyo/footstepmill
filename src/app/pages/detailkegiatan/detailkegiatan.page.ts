import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-detailkegiatan',
  templateUrl: './detailkegiatan.page.html',
  styleUrls: ['./detailkegiatan.page.scss'],
})

export class DetailkegiatanPage implements OnInit {

  stasiun = null;
  listData = [];

  constructor(private activatedRoute: ActivatedRoute,
    private storageService: StorageService) { }

  ngOnInit() {
    this.stasiun = this.activatedRoute.snapshot.paramMap.get('stasiun');
    // Get the information from the API
    this.storageService.getListKegiatanDetail(this.stasiun).then(items => {
      if(items!=null){
        this.listData = items.reverse();
      }
    });
  }

}
