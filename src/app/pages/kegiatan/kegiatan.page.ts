import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { StorageService } from '../../services/storage.service';
import { ToastController} from '@ionic/angular';
import 'rxjs/add/operator/timeout';
import { LoadingService } from '../../services/loading.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-kegiatan',
  templateUrl: './kegiatan.page.html',
  styleUrls: ['./kegiatan.page.scss'],
})

export class KegiatanPage implements OnInit {  
  url: string;
  dataStasiun: any = [];
  status: boolean;
  mill: string;
  jabatan: string;
  department: string;
  stasiun: string;

  constructor(private storage: Storage, private storageService: StorageService, private toastController: ToastController,
    public http: HttpClient,
    public loading: LoadingService,
    private navCtrl: NavController) { 
    this.storage.get('ipServer').then((val) => {
      this.url= val.url;
    });
    this.storage.get('userInfo').then((val) => {
      this.mill= val.mill;
      this.jabatan= val.jabatan;
      this.department= val.department;
      this.status= val.stasiun;
    });
  }

  ngOnInit() {
    this.loadListStasiun();
  }

  loadListStasiun(){
    this.storageService.getListStasiun().then(items => {
      if(items!=null){
        this.dataStasiun = items.reverse();
        console.log(this.dataStasiun)
      }
    });
  }

  updateData() {
    this.loading.present();
    this.updateStasiun().then(_=>{
      if(this.status){
        this.updateKegiatan();
      }
      this.loadListStasiun();
    });
    this.loading.dismiss();
  }

  updateStasiun(){
    var link = this.url+'/restapi/fsmill/load_stasiun';    
    return new Promise(resolve => {
      this.http.get(link, {headers: {'Auth-Key':'karyam4s', 'mill':this.mill}})
      .timeout(10000).subscribe((data:any) => {
        this.storage.set("stasiun", JSON.parse(JSON.stringify(data)));
        this.status = true;
        this.showToast("Stasiun berhasil diupdate");
        resolve();
      }, error => {
        this.status = false;
        this.showToast("Data gagal diupdate, cek koneksi jaringan");
        resolve();
      });
    });
  }

  updateKegiatan(){
    var link = this.url+'/restapi/fsmill/load_kegiatan';
    let status = true;
    return new Promise(resolve => {
        this.http.get(link, {headers: {'Auth-Key':'karyam4s', 'mill':this.mill, 'jabatan':this.jabatan, 'department':this.department}})
        .timeout(10000).subscribe((data:any) => {
        this.storage.set("kegiatan", JSON.parse(JSON.stringify(data)));
        this.status = true;
        this.showToast("Kegiatan berhasil diupdate");
        resolve();
      }, error => {
        this.status = false;
        this.showToast("Data gagal diupdate, cek koneksi jaringan");
        resolve();
      })
    });
  }

  detailPage(stasiun:any) {
    this.navCtrl.navigateRoot('detailkegiatan/'+stasiun);
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'middle',
    });
    toast.present();
  }
}
