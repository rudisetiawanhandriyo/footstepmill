import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KegiatanPage } from './kegiatan.page';

describe('KegiatanPage', () => {
  let component: KegiatanPage;
  let fixture: ComponentFixture<KegiatanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KegiatanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KegiatanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
