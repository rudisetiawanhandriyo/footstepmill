import { Component, OnInit } from '@angular/core';
import { StorageService, Item } from '../../services/storage.service';
import { ActivatedRoute } from '@angular/router';
import { ToastController, IonList } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { LoadingService } from '../../services/loading.service';

import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

export interface Assignment {
  driver: string;
  unit: string;
  arrival: string;
}

@Component({
  selector: 'app-update-data',
  templateUrl: './update-data.page.html',
  styleUrls: ['./update-data.page.scss'],
})

export class UpdateDataPage  {

  nama: string;
  userId: string;
  jabatan: string;
  mill: string;
  waktuTransaksi: string;
  transDate = null;
  createdDt = null;
  latitude: number;
  longitude: number;

  dataItem = null;
  item: Item[] = [];
  newItem: Item = <Item>{};
  form: FormGroup;
  assignmentList: FormArray;
  temuanList: FormArray;
  list: Assignment[] = [];
  idData = null;

  tanggal2: any;
  originalImage = null;
  blobImage = null;

  listFoto:string[]=[];
  listFotoView:string[]=[];
  listFotoTemuan:string[]=[];
  listFotoTemuanView:string[]=[];
  
  constructor(private storageService: StorageService, private activatedRoute: ActivatedRoute, private toastController: ToastController,
    private fb: FormBuilder, private navCtrl: NavController, 
    private camera: Camera,
    private photoViewer: PhotoViewer,
    public loading: LoadingService,
    private file: File,
    private webview: WebView) {
      this.tanggal2 = new Date();
      this.form = this.fb.group({
        stasiun: [null, Validators.compose([Validators.required])],
        transDate: [null, Validators.compose([Validators.required])],
        assignment: this.fb.array([]),
        temuan: this.fb.array([])
      });
  }

  //create formgroup with data
  createForms(assignment): FormGroup {
    let formGroup: FormGroup = new FormGroup(
      {
        'id': new FormControl(assignment.id, Validators.required),
        'kegiatan': new FormControl(assignment.kegiatan, Validators.required),
        'kondisi': new FormControl(assignment.kondisi, Validators.required),
        'keterangan': new FormControl(assignment.keterangan, Validators.required),
        'photo': new FormControl(assignment.photo, Validators.required),
      }
    );
    return formGroup;
  }

  // get the formgroup under contacts form array
  getAssignmentsFormGroup(index): FormGroup {
    const formGroup = this.assignmentList.controls[index] as FormGroup;
    return formGroup;
  }

  // bagian temuan
  createFormTemuan(temuan): FormGroup{
    let formGroup: FormGroup = new FormGroup({
      'keterangan': new FormControl(temuan.keterangan, Validators.required),
      'photo': new FormControl(temuan.photo, Validators.required),
    });
    return formGroup;
  }

  createTemuan(): FormGroup{
    return this.fb.group({
      keterangan: [null, Validators.compose([Validators.required])], 
      photo: [null, Validators.compose([Validators.required])], 
    });
  }
  
  addTemuan(){
    this.temuanList.push(this.createTemuan());
  }

  removeTemuan(i: number) : void{
    const control = <FormArray>this.form.get('temuan');
    control.removeAt(i);
  }

  // get the formgroup under contacts form array
  getTemuanFormGroup(index): FormGroup {
    const formGroup = this.temuanList.controls[index] as FormGroup;
    return formGroup;
  }

  ionViewWillEnter(){
    this.startAwal();
  }

  ionViewWillLeave(){
    this.form.reset();
    const control = <FormArray>this.form.get('assignment');
    for(let i = control.length-1; i >= 0; i--) {
      control.removeAt(i)
    }
    const controlTemuan = <FormArray>this.form.get('temuan');
    for(let i = controlTemuan.length-1; i >= 0; i--) {
      controlTemuan.removeAt(i)
    }
  }

  startAwal() {
    // set datalist to this field
    this.assignmentList = this.form.get('assignment') as FormArray;
    this.temuanList = this.form.get('temuan') as FormArray;
    // Get the ID that was passed with the URL
    this.idData = this.activatedRoute.snapshot.paramMap.get('id');
    this.storageService.getItem(this.idData).then((item : Item[]) => {
      this.userId = item[0].userId;
      this.nama = item[0].nama;
      this.jabatan = item[0].jabatan;
      this.mill = item[0].mill;
      this.transDate = item[0].transDate;
      this.createdDt = item[0].createdDt;
      this.latitude = item[0].latitude;
      this.longitude = item[0].longitude;
      
      var date = new Date(parseInt(item[0].transDate));
      var year = date.getFullYear();
      var month = ("0" + (date.getMonth() + 1)).slice(-2);
      var day = ("0" + date.getDate()).slice(-2);
      var jam = ("0" + date.getHours()).slice(-2);
      var menit = ("0" + date.getMinutes()).slice(-2);
      this.waktuTransaksi = day+'-'+month+'-'+year+' / '+jam +':'+menit;

      this.form.patchValue({
        stasiun: item[0].stasiun,
        transDate: item[0].transDate,
        assignment: [],
        temuan: [],
      })
      var assignmentData = item[0].checklist;
      if(assignmentData.length!=0){
        var obj = JSON.parse(JSON.stringify(assignmentData));
        const formArray = this.form.get('assignment') as FormArray;
        let i = 0;
        obj.forEach(item => {
          formArray.push(this.createForms(item));
          let filePath = this.file.dataDirectory + item.photo;
          let resPath = this.pathForImage(filePath);
          if(item.photo!='-'){
            this.listFoto[i] = resPath;
            this.listFotoView[i] = filePath;
          }
          i++;
        });
      }
      var temuanData = item[0].temuan;
      if(temuanData != undefined){
        var obj = JSON.parse(JSON.stringify(temuanData));
        const formTemuanArray = this.form.get('temuan') as FormArray;
        let i = 0;
        obj.forEach(item => {
          formTemuanArray.push(this.createFormTemuan(item));
          let filePath = this.file.dataDirectory + item.photo;
          let resPath = this.pathForImage(filePath);
          this.listFotoTemuan[i] = resPath;
          this.listFotoTemuanView[i] = filePath;
          i++;
        });
      }
    });
  }

  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  kondisiChange(index: string, event: any){
    var kondisi = event.target.value;
    if(kondisi=="yes" || kondisi=="skip"){
      this.assignmentList.controls[index].controls['keterangan'].setValue("-");
      this.assignmentList.controls[index].controls['photo'].setValue("-");
    }else{
      this.assignmentList.controls[index].controls['keterangan'].setValue("");
      this.assignmentList.controls[index].controls['photo'].setValue("");
    }
  }

  // UPDATE
  updateItem(form) {
    this.loading.present();
    this.newItem.id = parseInt(this.idData);
    this.newItem.stasiun = form.value.stasiun;
    this.newItem.transDate = this.transDate;
    this.newItem.mill = this.mill;
    this.newItem.nama = this.nama;
    this.newItem.jabatan = this.jabatan;   
    this.newItem.userId = this.userId;   
    this.newItem.latitude = this.latitude;
    this.newItem.longitude = this.longitude;
    this.newItem.checklist = form.value.assignment;
    this.newItem.temuan = form.value.temuan;
    this.newItem.createdDt = this.createdDt;
    this.newItem.updatedDt = Date.now();
    this.newItem.submitDt = 0;
    this.newItem.flag = "0";
    
    this.storageService.updateItem(this.newItem).then(item => {
      this.loading.dismiss();
      this.showToast('Data berhasil diupdate');
    });
  }

  //generate filename foto
  createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName = n + this.userId + ".jpg";
    return newFileName;
  }

  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  //mengambil foto
  takePhoto(index: string) {
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: false,
      allowEdit : false,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.originalImage = 'data:image/jpeg;base64,' + imagePath;
      let nameImage = this.createFileName();
      this.listFoto[index] = this.originalImage;
      this.listFotoView[index] = this.file.dataDirectory + nameImage;
      let realData = this.originalImage.split(",")[1];
      let blob = this.b64toBlob(realData, 'image/jpeg');

      this.file.writeFile(this.file.dataDirectory, nameImage, blob).then(success => {
        this.assignmentList.controls[index].controls['photo'].setValue(nameImage);
      }, error => {
        this.showToast('Error while storing file.');
      });
    }, (err) => {
      // Handle error
      console.log("Camera issue:" + err);
    });
  }  

  //mengambil foto
  takePhotoTemuan(index: string) {
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      allowEdit : false,
    };
    this.camera.getPicture(options).then((imagePath) => {
      this.originalImage = 'data:image/jpeg;base64,' + imagePath;
      let nameImage = this.createFileName();
      this.listFotoTemuan[index] = this.originalImage;
      this.listFotoTemuanView[index] = this.file.dataDirectory + nameImage;
      let realData = this.originalImage.split(",")[1];
      let blob = this.b64toBlob(realData, 'image/jpeg');

      this.file.writeFile(this.file.dataDirectory, nameImage, blob).then(success => {
        this.temuanList.controls[index].controls['photo'].setValue(nameImage);
      }, error => {
        this.showToast('Error while storing file.');
      });
    }, (err) => {
      // Handle error
      console.log("Camera issue:" + err);
    });
  }  

  viewPhotoTemuan(index: string) {
    this.photoViewer.show(this.listFotoTemuanView[index]);
  }

  viewPhoto(index: string) {
    this.photoViewer.show(this.listFotoView[index]);
  }

  // GO BACK
  goBack(){
    this.navCtrl.pop();
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000, 
    });
    toast.present();
  }

}
