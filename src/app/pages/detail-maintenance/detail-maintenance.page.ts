import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-detail-maintenance',
  templateUrl: './detail-maintenance.page.html',
  styleUrls: ['./detail-maintenance.page.scss'],
})
export class DetailMaintenancePage implements OnInit {

  stasiun = null;
  listData = [];

  constructor(private activatedRoute: ActivatedRoute,
    private storageService: StorageService,
    private photoViewer: PhotoViewer) { }

  ngOnInit() {
    this.stasiun = this.activatedRoute.snapshot.paramMap.get('stasiun');
    // Get the information from the API
    this.storageService.getListTagDetailMaintenance(this.stasiun).then(items => {
      if(items!=null){
        this.listData = items.reverse();
      }
    });
  }

  viewFoto(src){
    this.photoViewer.show(src);
  }

}
