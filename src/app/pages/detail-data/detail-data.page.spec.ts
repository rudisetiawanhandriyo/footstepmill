import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailDataPage } from './detail-data.page';

describe('DetailDataPage', () => {
  let component: DetailDataPage;
  let fixture: ComponentFixture<DetailDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailDataPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
