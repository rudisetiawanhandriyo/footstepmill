import { Component, OnInit } from '@angular/core';
import { StorageService, Item } from '../../services/storage.service';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-derail-data',
  templateUrl: './detail-data.page.html',
  styleUrls: ['./detail-data.page.scss'],
})

export class DetailDataPage  {

  nama: string;
  userId: string;
  jabatan: string;
  mill: string;
  waktuTransaksi: string;
  transDate = null;
  createdDt = null;

  item: Item[] = [];
  newItem: Item = <Item>{};
  form: FormGroup;
  assignmentList: FormArray;
  temuanList: FormArray;
  statusTemuan: boolean;
  idData = null;

  listFoto:string[]=[];
  listFotoView:string[]=[];
  listFotoTemuan:string[]=[];
  listFotoTemuanView:string[]=[];

  constructor(private storageService: StorageService, private activatedRoute: ActivatedRoute, private toastController: ToastController,
    private fb: FormBuilder, private navCtrl: NavController,
    private photoViewer: PhotoViewer,
    private file: File,
    private webview: WebView) {
      this.form = this.fb.group({
        stasiun: [null, Validators.compose([Validators.required])],
        transDate: [null, Validators.compose([Validators.required])],
        assignment: this.fb.array([]),
        temuan: this.fb.array([])
      });
  }

  //create form with data
  createForms(assignment): FormGroup {
    let formGroup: FormGroup = new FormGroup(
      {
        'kegiatan': new FormControl(assignment.kegiatan, Validators.required),
        'kondisi': new FormControl(assignment.kondisi, Validators.required),
        'keterangan': new FormControl(assignment.keterangan, Validators.required),
        'photo': new FormControl(assignment.photo, Validators.required)
      }
    );
    return formGroup;
  }

  // get the formgroup under contacts form array
  getAssignmentsFormGroup(index): FormGroup {
    const formGroup = this.assignmentList.controls[index] as FormGroup;
    return formGroup;
  }

  // bagian temuan
  createFormTemuan(temuan): FormGroup{
    let formGroup: FormGroup = new FormGroup({
      'keterangan': new FormControl(temuan.keterangan, Validators.required),
      'photo': new FormControl(temuan.photo, Validators.required),
    });
    return formGroup;
  }

  getTemuanFormGroup(index): FormGroup {
    const formGroup = this.temuanList.controls[index] as FormGroup;
    return formGroup;
  }

  ionViewWillEnter(){
    this.startAwal();
  }

  ionViewWillLeave(){
    this.form.reset();
    const control = <FormArray>this.form.get('assignment');
    for(let i = control.length-1; i >= 0; i--) {
      control.removeAt(i)
    }
    const controlTemuan = <FormArray>this.form.get('temuan');
    for(let i = controlTemuan.length-1; i >= 0; i--) {
      controlTemuan.removeAt(i)
    }
  }

  startAwal() {
    // set datalist to this field
    this.assignmentList = this.form.get('assignment') as FormArray;
    this.temuanList = this.form.get('temuan') as FormArray;
    // Get the ID that was passed with the URL
    this.idData = this.activatedRoute.snapshot.paramMap.get('id');
    // Get the information from the API
    this.storageService.getItem(this.idData).then((item : Item[]) => {
      this.userId = item[0].userId;
      this.nama = item[0].nama;
      this.jabatan = item[0].jabatan;
      this.mill = item[0].mill;
      this.transDate = item[0].transDate;
      
      var date = new Date(parseInt(item[0].transDate));
      var year = date.getFullYear();
      var month = ("0" + (date.getMonth() + 1)).slice(-2);
      var day = ("0" + date.getDate()).slice(-2);
      var jam = ("0" + date.getHours()).slice(-2);
      var menit = ("0" + date.getMinutes()).slice(-2);
      this.waktuTransaksi = day+'-'+month+'-'+year+' / '+jam +':'+menit;

      this.form.patchValue({
        stasiun: item[0].stasiun,
        transDate: item[0].transDate,
        checklist: [],
        temuan: []
      })
      var assignmentData = item[0].checklist;
      if(assignmentData.length!=0){
        var obj = JSON.parse(JSON.stringify(assignmentData));
        const formArray = this.form.get('assignment') as FormArray;
        let i = 0;
        obj.forEach(item => {
          formArray.push(this.createForms(item));
          let filePath = this.file.dataDirectory + item.photo;
          let resPath = this.pathForImage(filePath);
          if(item.photo!='-'){
            this.listFoto[i] = resPath;
            this.listFotoView[i] = filePath;
          }
          i++;
        });
      }
      var temuanData = item[0].temuan;
      this.statusTemuan = false;
      if(temuanData != undefined){
        this.statusTemuan = true;
        var obj = JSON.parse(JSON.stringify(temuanData));
        const formTemuanArray = this.form.get('temuan') as FormArray;
        let i = 0;
        obj.forEach(item => {
          formTemuanArray.push(this.createFormTemuan(item));
          let filePath = this.file.dataDirectory + item.photo;
          let resPath = this.pathForImage(filePath);
          this.listFotoTemuan[i] = resPath;
          this.listFotoTemuanView[i] = filePath;
          i++;
        });
      }
    });
  }

  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
  
  viewPhoto(index: string) {
    this.photoViewer.show(this.listFotoView[index]);
  }

  viewPhotoTemuan(index: string) {
    this.photoViewer.show(this.listFotoTemuanView[index]);
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000, 
    });
    toast.present();
  }

}
