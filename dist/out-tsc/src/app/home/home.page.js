import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { Platform, ToastController, IonList } from '@ionic/angular';
var HomePage = /** @class */ (function () {
    function HomePage(storageService, plt, toastController) {
        var _this = this;
        this.storageService = storageService;
        this.plt = plt;
        this.toastController = toastController;
        this.items = [];
        this.newItem = {};
        this.currentStatus = "add";
        this.plt.ready().then(function () {
            _this.loadItems();
        });
    }
    // CREATE
    HomePage.prototype.addItem = function () {
        var _this = this;
        this.newItem.modified = Date.now();
        this.newItem.id = Date.now();
        this.storageService.addItem(this.newItem).then(function (item) {
            _this.newItem = {};
            _this.showToast('Item added!');
            _this.loadItems(); // Or add it to the array directly
        });
    };
    // READ
    HomePage.prototype.loadItems = function () {
        var _this = this;
        this.storageService.getItems().then(function (items) {
            _this.items = items;
        });
    };
    // GET ITEM
    HomePage.prototype.getItem = function (item) {
        this.newItem.id = item.id;
        this.newItem.title = item.title;
        this.newItem.value = item.value;
        this.currentStatus = "update";
        this.mylist.closeSlidingItems();
    };
    // UPDATE
    HomePage.prototype.updateItem = function () {
        var _this = this;
        this.storageService.updateItem(this.newItem).then(function (item) {
            _this.newItem = {};
            _this.showToast('Item updated!');
            _this.mylist.closeSlidingItems(); // Fix or sliding is stuck afterwards
            _this.loadItems(); // Or update it inside the array directly
        });
    };
    // DELETE
    HomePage.prototype.deleteItem = function (item) {
        var _this = this;
        this.storageService.deleteItem(item.id).then(function (item) {
            _this.showToast('Item removed!');
            _this.mylist.closeSlidingItems(); // Fix or sliding is stuck afterwards
            _this.loadItems(); // Or splice it from the array directly
        });
    };
    // Helper
    HomePage.prototype.showToast = function (msg) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: msg,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib_1.__decorate([
        ViewChild('mylist'),
        tslib_1.__metadata("design:type", IonList)
    ], HomePage.prototype, "mylist", void 0);
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [StorageService, Platform, ToastController])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map