import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
var ITEMS_KEY = 'my-items';
var StorageService = /** @class */ (function () {
    function StorageService(storage) {
        this.storage = storage;
    }
    // CREATE
    StorageService.prototype.addItem = function (item) {
        var _this = this;
        return this.storage.get(ITEMS_KEY).then(function (items) {
            if (items) {
                items.push(item);
                return _this.storage.set(ITEMS_KEY, items);
            }
            else {
                return _this.storage.set(ITEMS_KEY, [item]);
            }
        });
    };
    // READ
    StorageService.prototype.getItems = function () {
        return this.storage.get(ITEMS_KEY);
    };
    // UPDATE
    StorageService.prototype.updateItem = function (item) {
        var _this = this;
        return this.storage.get(ITEMS_KEY).then(function (items) {
            if (!items || items.length === 0) {
                return null;
            }
            var newItems = [];
            for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
                var i = items_1[_i];
                if (i.id === item.id) {
                    newItems.push(item);
                }
                else {
                    newItems.push(i);
                }
            }
            return _this.storage.set(ITEMS_KEY, newItems);
        });
    };
    // DELETE
    StorageService.prototype.deleteItem = function (id) {
        var _this = this;
        return this.storage.get(ITEMS_KEY).then(function (items) {
            if (!items || items.length === 0) {
                return null;
            }
            var toKeep = [];
            for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
                var i = items_2[_i];
                if (i.id !== id) {
                    toKeep.push(i);
                }
            }
            return _this.storage.set(ITEMS_KEY, toKeep);
        });
    };
    StorageService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Storage])
    ], StorageService);
    return StorageService;
}());
export { StorageService };
//# sourceMappingURL=storage.service.js.map